#!/usr/bin/env python
import numpy as np
import math
from scipy.linalg import expm
from lab2_header import *

"""
Use 'expm' for matrix exponential.
Angles are in radian, distance are in meters.
"""

def create_S(w,v):
    nW1 = np.multiply(w[0],-1)
    nW2 = np.multiply(w[1],-1)
    nW3 = np.multiply(w[2],-1)
    
    S = np.array([[0, nW3 , w[1], v[0]],
                 [w[2], 0, nW1, v[1]],
                 [nW2, w[0], 0, v[2]],
                 [0, 0, 0, 0]])
    return S
    
def Get_MS():
	# =================== Your code starts here ====================#
	# Fill in the correct values for a1~6 and q1~6, as well as the M matrix
	M = np.eye(4)
	S = np.zeros((6,6))
	z = .162
	w = [[0,0,1],[0,1,0],[0,1,0],[0,1,0],[1,0,0],[0,1,0]]
	q = [[-.15,.15,z], [-.15,.120+.15,z],[.244-.15,.120+.15,z],[.457-.15,.027+.15,z],[.457-.15,.15+.110,z],[.54-.15,.192+.15,z]]
	S=[]
	for i in range(len(q)):
		v = np.cross(np.multiply(w[i], -1), q[i])
		S.append(create_S(w[i],v))
  
	M = np.array([[0, -1, 0, .540-.15], 
              	 [0, 0, -1, .192+.15+.059],
                 [1, 0, 0, .162+.0535],
                 [0, 0, 0, 1]])



	# ==============================================================#
	return M, S

"""
Function that calculates encoder numbers for each motor
"""
def lab_fk(theta1, theta2, theta3, theta4, theta5, theta6):

	# Initialize the return_value
	return_value = [None, None, None, None, None, None]

	#print("Foward kinematics calculated:\n")

	# =================== Your code starts here ====================#
	theta = np.array([theta1,theta2,theta3,theta4,theta5,theta6])
	T = np.eye(4)

	M, S = Get_MS()
	T = np.dot(np.dot(np.dot(np.dot(np.dot(np.dot(expm(S[0]*theta1), expm(S[1]*theta2)), expm(S[2]*theta3)), expm(S[3]*theta4)), expm(S[4]*theta5)), expm(S[5]*theta6)), M)
	#print(T)



	# ==============================================================#

	return_value[0] = theta1 + PI
	return_value[1] = theta2
	return_value[2] = theta3
	return_value[3] = theta4 - (0.5*PI)
	return_value[4] = theta5
	return_value[5] = theta6

	return return_value


"""
Function that calculates an elbow up Inverse Kinematic solution for the UR3
"""
def lab_invk(xWgrip, yWgrip, zWgrip, yaw_WgripDegree):
	# =================== Your code starts here ====================#
	l10 = .059
	l9 = .0535
	l8 = .082
	l7 = 0.083
	l6 = 0.083
	l5 = .213
	l4 = 0.093
	l3 = .244
	l2 = 0.12
	l1 = .152
	xgrip = xWgrip + .15
	ygrip = yWgrip - .15
	zgrip = zWgrip - .01

	xcen = xgrip - l9* np.cos(np.radians(yaw_WgripDegree))
	ycen = ygrip - l9* np.sin(np.radians(yaw_WgripDegree))
	zcen = zgrip
 
	lxyc = np.sqrt((xcen**2)+(ycen**2)) #length of xcen , ycen
	thetam1 = np.arcsin((l2 - l4 + l6)/lxyc)
	theta1 = np.arctan2(ycen, xcen) - thetam1 
	theta6 = np.radians((90 - yaw_WgripDegree)) + theta1 
 
	l3mid = np.sqrt((xcen**2) + (ycen**2) - (l2 - l4 + l6)**2) - l7 #length of end vector

	x3end = l3mid * np.cos(theta1)
	y3end = l3mid * np.sin(theta1)
	z3end = zcen + l8 + l10
 
	lxye = np.sqrt((x3end**2)+(y3end**2))
	
	l2mid = np.sqrt((x3end**2) + (y3end**2) + ((z3end - l1)**2)) #length of law of cos triangle from l1 to xend,yend
	theta2a = np.arctan2((z3end - l1), lxye)
	theta2b = np.arccos(((l2mid**2) + (l3**2) - (l5**2)) / (2 * l2mid * l3))
	theta2 = (-1) * (theta2a + theta2b)
 
	theta3mid = np.arccos(((l3**2) + (l5**2) - (l2mid**2))/ (2 * l3 * l5))
	theta3 = np.pi - theta3mid 
	theta4 = -theta3 - theta2
	theta5 = -1 * np.pi/2
	#print("T1:",theta1, "\t T2:", theta2, "\t T3:",theta3)
	#print("T4:",theta4, "\t T5:", theta5, "\t T6:",theta6)
	
	# ==============================================================#
	return lab_fk(theta1, theta2, theta3, theta4, theta5, theta6)
