#!/usr/bin/env python3

import cv2
import numpy as np

# ========================= Student's code starts here =========================

# Params for camera calibration
Or = 240 #15+240
Oc = 320 #53+320
theta = 0 #-1*np.radians(1.5482)  # degrees
beta = 723.9537   # pix/m
tx = -0.28 #.25 - ((421-Or)/beta)
ty = -0.095 #.25 - ((218-Oc)/beta)
R = np.array([[np.cos(theta), -np.sin(theta)],[np.sin(theta),np.cos(theta)]])
r_inv = np.linalg.inv(R)
# Function that converts image coord to world coord
# QUANG IDT THIS IS RIGHT
def IMG2W(col, row):
    xc = float ((row - Or) / beta)#(row - Or) / beta
    yc = float ((col - Oc) / beta)
    xw = xc - tx
    yw = yc - ty
    xw_yw_matrix = np.array([xw,yw])
    temp_ans = np.matmul(r_inv,xw_yw_matrix)
    zw = 0.0318 # WILL BE CONSTANT, MUST MEASURE
    temp_ans = np.append(temp_ans, zw)
    
    # l = np.sqrt((xw**2) + yw**2)
    # thetar = np.arctan(xw/yw) - theta
    # xwr = l * np.sin(thetar)
    # ywr = l * np.cos(thetar)
   
    return temp_ans

# ========================= Student's code ends here ===========================

def blob_search(image_raw, color):

    # Setup SimpleBlobDetector parameters.
    params = cv2.SimpleBlobDetector_Params()

    # ========================= Student's code starts here =========================

    # Filter by Color
    params.filterByColor = False
    params.blobColor = 255

    # Filter by Area.
    params.filterByArea = True
    params.minArea = 90
    params.maxArea = 3000

    # Filter by Circularity
    params.filterByCircularity = False
    # params.minCircularity = 0.4
    # params.maxCircularity = 1
    
    # Filter by Inerita
    params.filterByInertia = False

    # Filter by Convexity
    params.filterByConvexity = False

    # ========================= Student's code ends here ===========================

    # Create a detector with the parameters
    detector = cv2.SimpleBlobDetector_create(params)

    # Convert the image into the HSV color space
    hsv_image = cv2.cvtColor(image_raw, cv2.COLOR_BGR2HSV)
    # print(color)

    # ========================= Student's code starts here =========================
    if color == "safe":
        lower = (30,100, 50)
        upper = (90, 255, 255)
    if color == "sheep":
        lower = (0,0,250)     
        upper = (0,0,255) 
    # CALCULATE THESE VALUES DEPENDING ON WHAT COLOR WE WANT
    if color == "wolf":
        lower = (0,0,0)     
        upper = (0,0,10)  
    if color == "jail":
        lower = (0,240,240)
        upper = (0,255,255)
  
    # Define a mask using the lower and upper bounds of the target color
    mask_image =  cv2.inRange(hsv_image, lower, upper)
    # ========================= Student's code ends here ===========================

    keypoints = detector.detect(mask_image)
    
    # Find blob centers in the image coordinates
    blob_image_center = []
    num_blobs = len(keypoints)
    for i in range(num_blobs):
        blob_image_center.append((keypoints[i].pt[0],keypoints[i].pt[1]))

    # ========================= Student's code starts here =========================

    # Draw the keypoints on the detected block
    im_with_keypoints = cv2.drawKeypoints(image_raw, keypoints, 0, (255, 0, 0) )

    # ========================= Student's code ends here ===========================

    xw_yw_zw = []

    if(num_blobs == 0):
        #print("No block found!")
        hello = 0
    else:
        # Convert image coordinates to global world coordinate using IM2W() function
        for i in range(num_blobs):
            xw_yw_zw.append(IMG2W(blob_image_center[i][0], blob_image_center[i][1]))


    cv2.namedWindow("Camera View")
    cv2.imshow("Camera View", image_raw)
    cv2.namedWindow("Mask View")
    cv2.imshow("Mask View", mask_image)
    cv2.namedWindow("Keypoint View")
    cv2.imshow("Keypoint View", im_with_keypoints)

    cv2.waitKey(2)
    # return None
    return xw_yw_zw
