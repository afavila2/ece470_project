# ECE470_Project

## Name
Team Sheeeeeep: Herding Sheep Robot

## Description
For our pick and place challenge, we implemented a sheep protector robot. The motivation for our robot was to face the challenge of rescuing grazing sheep that are in direct danger of wolves. Our simulation will randomly spawn in all of the desired sheep and wolves into the area by the robot, as well as spawn in green sheep safe zones and red wolf removal zones. Our robot will detect the proximity of all the individual sheep to the wolves, and then start picking and placing the sheep to the nearest free safe zone in order of danger level. After rescuing all of the sheep, the robot will then commence the wolf removal process and move the wolves to the removal zones.


## Features
Random spawning of all objects

Optical Detection of "sheep", "wolves", "safe zones", and "wolf zones" on the field

Inverse kinematics algorithm to calculate angles needed to orient the robot to pick up the sheep in the field

Detection of whether a sheep was successfully grabbed, then placed onto a safe zone

Threat priority and movement of high risk sheep

Sheep that are closest to the most wolves will be moved first

All sheep are moved to the closest free safe zone

## Visuals
Video Demo Link for Project Update 1: https://youtu.be/tvmmeVmiGDM
Video Demo Link for Project Update 2: https://youtu.be/2zC53OIaVsg 
Final Video Demo Link: https://drive.google.com/file/d/1laoR3BC1FZ6jgd8kD2WLP5ARBtoNYR31/view?usp=share_link 

## Project History:
12/06/22:
- Implemented random spawning of
    - sheep
    - wolves
    - safe zones
- Implemented distance detection of sheep to wolves
- Implemented distance detection of sheep to closest open safe zones
- Able to place the picked sheep directly onto the safe zone blocks
- Finished removing the wolves from the sheep area

11/20/22:
- Able to put multiple blocks/animals into task grid
- Successfully implemented forward kinematics within the pick and place feature
- Progress with object detection (as seen in object_detection branch)
- Implemented threat priority and movement of high risk sheep logic
- Located package files to upload farm world background
- Updated relevant files

10/16/22: 
-Created Gazebo VM and Imported Lab 2 Code into provided Code. 
-Successfully conveyed movement into the simulated robot as well as receieved analog in/out of the gripper. 
-Encountered issue where positions and commands being fed into the simulator were not correctly interepreted resulting in type errors. 
-Created/Updated Project Update Documents

    


## Authors
Amanda Favila, Quang Nguyen
