#!/usr/bin/env python

'''
We get inspirations of Tower of Hanoi algorithm from the website below.
This is also on the lab manual.
Source: https://www.cut-the-knot.org/recurrence/hanoi.shtml
'''

import os
import argparse
import copy
import time
import rospy
import rospkg
import numpy as np
import yaml
import sys
from lab2_header import *
from lab2_func import *
import math
from scipy.linalg import expm
from blob_search import *

from ur3_driver.msg import command
from ur3_driver.msg import position
from ur3_driver.msg import gripper_input

from std_msgs.msg import String
from sensor_msgs.msg import Image
from geometry_msgs.msg import Point
from cv_bridge import CvBridge, CvBridgeError

# 20Hz
SPIN_RATE = 20

# Position for UR3 not blocking the camera
go_away = [270*PI/180.0, -90*PI/180.0, 90*PI/180.0, -90*PI/180.0, -90*PI/180.0, 135*PI/180.0]

# Store world coordinates of sheep and wolf blocks
xw_yw_S = []
xw_yw_W = []
xw_yw_G = []
xw_yw_R = []
# Any other global variable you want to define
# Hints: where to put the blocks?
#orange_dest = [[],[]]
#sheep_dest = [[.1,0.4,.04],[.1,0.15,.04]]
#wolf_dest = [[-.05,0.4,.04],[-.05,0.15,.04]]
pause = 0
# UR3 home location
home = np.radians([120, -90, 90, -90, -90, 0])

# UR3 current position, using home position for initialization
current_position = copy.deepcopy(home)

thetas = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

digital_in_0 = 0
analog_in_0 = 0
analog_in_1 = 0
suction_on = True
suction_off = False

current_io_0 = False
current_position_set = False

Q = None

############## Your Code Start Here ##############

"""
TODO: define a ROS topic callback funtion for getting the state of suction cup
Whenever ur3/gripper_input publishes info this callback function is called.
"""
def topic_callback(msg):

    global digital_in_0
    global analog_in_0
    global analog_in_1
    digital_in_0 = msg.DIGIN
    analog_in_0 = msg.AIN0
    analog_in_1 = msg.AIN1



############### Your Code End Here ###############


"""
Whenever ur3/position publishes info, this callback function is called.
"""
def position_callback(msg):

    global thetas
    global current_position
    global current_position_set

    thetas[0] = msg.position[0]
    thetas[1] = msg.position[1]
    thetas[2] = msg.position[2]
    thetas[3] = msg.position[3]
    thetas[4] = msg.position[4]
    thetas[5] = msg.position[5]

    current_position[0] = thetas[0]
    current_position[1] = thetas[1]
    current_position[2] = thetas[2]
    current_position[3] = thetas[3]
    current_position[4] = thetas[4]
    current_position[5] = thetas[5]

    current_position_set = True


def gripper(pub_cmd, loop_rate, io_0):

    global SPIN_RATE
    global thetas
    global current_io_0
    global current_position

    error = 0
    spin_count = 0
    at_goal = 0

    current_io_0 = io_0

    driver_msg = command()
    driver_msg.destination = current_position
    driver_msg.v = 1.0
    driver_msg.a = 1.0
    driver_msg.io_0 = io_0
    pub_cmd.publish(driver_msg)

    while(at_goal == 0):

        if( abs(thetas[0]-driver_msg.destination[0]) < 0.0005 and \
            abs(thetas[1]-driver_msg.destination[1]) < 0.0005 and \
            abs(thetas[2]-driver_msg.destination[2]) < 0.0005 and \
            abs(thetas[3]-driver_msg.destination[3]) < 0.0005 and \
            abs(thetas[4]-driver_msg.destination[4]) < 0.0005 and \
            abs(thetas[5]-driver_msg.destination[5]) < 0.0005 ):

            at_goal = 1

        loop_rate.sleep()

        if(spin_count >  SPIN_RATE*5):

            pub_cmd.publish(driver_msg)
            rospy.loginfo("Just published again driver_msg")
            spin_count = 0

        spin_count = spin_count + 1

    return error


def move_arm(pub_cmd, loop_rate, dest, vel, accel):

    global thetas
    global SPIN_RATE

    error = 0
    spin_count = 0
    at_goal = 0

    driver_msg = command()
    driver_msg.destination = dest
    driver_msg.v = vel
    driver_msg.a = accel
    driver_msg.io_0 = current_io_0
    pub_cmd.publish(driver_msg)

    loop_rate.sleep()

    while(at_goal == 0):

        if( abs(thetas[0]-driver_msg.destination[0]) < 0.0005 and \
            abs(thetas[1]-driver_msg.destination[1]) < 0.0005 and \
            abs(thetas[2]-driver_msg.destination[2]) < 0.0005 and \
            abs(thetas[3]-driver_msg.destination[3]) < 0.0005 and \
            abs(thetas[4]-driver_msg.destination[4]) < 0.0005 and \
            abs(thetas[5]-driver_msg.destination[5]) < 0.0005 ):

            at_goal = 1
            #rospy.loginfo("Goal is reached!")

        loop_rate.sleep()

        if(spin_count >  SPIN_RATE*5):

            pub_cmd.publish(driver_msg)
            rospy.loginfo("Just published again driver_msg")
            spin_count = 0

        spin_count = spin_count + 1

    return error


############## Your Code Start Here ##############

def move_block(pub_cmd, loop_rate, start_xw_yw_zw, target_xw_yw_zw, vel, accel):

    # ========================= Student's code starts here =========================

    # global variable1
    global home
    global go_away
    # global variable2
    global digital_in_0

    error = 0
    
    #convert start/target coord
    startCoord = lab_invk(start_xw_yw_zw[0],start_xw_yw_zw[1],start_xw_yw_zw[2],45)
    tUp = lab_invk(target_xw_yw_zw[0], target_xw_yw_zw[1], target_xw_yw_zw[2]+.05, 45)
    targetCoord = lab_invk(target_xw_yw_zw[0], target_xw_yw_zw[1], 0.015, 45)
    move_arm(pub_cmd, loop_rate, home, vel, accel)
    time.sleep(0.5)
    move_arm(pub_cmd, loop_rate, startCoord, vel, accel)
    time.sleep(.5)
    gripper(pub_cmd, loop_rate, suction_on)
    time.sleep(.5)
    # if digital_in_0 == 0:
    #     gripper(pub_cmd, loop_rate, suction_off)
    #     error = 1
    #     print("Missing Block")
    #     move_arm(pub_cmd, loop_rate, go_away, vel, accel)
    #     return error
    move_arm(pub_cmd, loop_rate, home, vel, accel)
    move_arm(pub_cmd, loop_rate, tUp, vel, accel)
    move_arm(pub_cmd, loop_rate, targetCoord, vel, accel)
    gripper(pub_cmd, loop_rate, suction_off)
    time.sleep(.5)
    move_arm(pub_cmd, loop_rate, go_away, vel, accel)
    
    error = 0

    # ========================= Student's code ends here ===========================

    return error
    ### Hint: Use the Q array to map out your towers by location and "height".

class ImageConverter:

    def __init__(self, SPIN_RATE):

        self.bridge = CvBridge()
        self.image_pub = rospy.Publisher("/image_converter/output_video", Image, queue_size=10)
        self.image_sub = rospy.Subscriber("/cv_camera_node/image_raw", Image, self.image_callback)
        self.loop_rate = rospy.Rate(SPIN_RATE)

        # Check if ROS is ready for operation
        while(rospy.is_shutdown()):
            print("ROS is shutdown!")


    def image_callback(self, data):

        global xw_yw_W # store found wolf blocks in this list
        global xw_yw_S # store found sheep blocks in this list
        global xw_yw_G # store found Safe blocks in this list
        global xw_yw_R # store found Safe blocks in this list
        global pause

        try:
          # Convert ROS image to OpenCV image
            raw_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
        except CvBridgeError as e:
            print(e)

        cv_image = cv2.flip(raw_image, -1)
        #cv2.line(cv_image, (0,50), (640,50), (0,0,0), 5) #black line

        # You will need to call blob_search() function to find centers of sheep blocks
        # and wolf blocks, and store the centers in xw_yw_S & xw_yw_W respectively.

        # If no blocks are found for a particular color, you can return an empty list,
        # to xw_yw_S or xw_yw_W.

        # Remember, xw_yw_S & xw_yw_W are in global coordinates, which means you will
        # do coordinate transformation in the blob_search() function, namely, from
        # the image frame to the global world frame.
        # xw_yw_O = blob_search(raw_image, "orange")
        #Sif pause == 0:
        xw_yw_S = blob_search(cv_image, "sheep")
        xw_yw_W = blob_search(cv_image, "wolf")
        xw_yw_G = blob_search(cv_image, "safe")
        xw_yw_R = blob_search(cv_image, "jail")

############### Your Code End Here ###############

#OUTDATED FUNCTION
# def from_to(sr, sc, er, ec, pub_command, loop_rate):
#     rospy.loginfo("Move above starting block ...")
#     move_arm(pub_command, loop_rate, Q[sr][sc][1], 4.0, 4.0)

#     gripper(pub_command, loop_rate, suction_on)
#     rospy.loginfo("Turning Gripper On and Waiting 0.5")
#     #Delay to make sure suction cup has grasped the block
#     time.sleep(0.1)

#     rospy.loginfo("Moving to block ...")
#     move_arm(pub_command, loop_rate, Q[sr][sc][0], 4.0, 4.0)

#     if digital_in_0 == 0:
#         rospy.loginfo("No Block Detected, Turning Gripper Off")
#         gripper(pub_command, loop_rate, suction_off)
#         rospy.loginfo("Going Home")
#         move_arm(pub_command, loop_rate, home, 4.0, 4.0)

#     if digital_in_0 == 1:
#         rospy.loginfo("Block Detected, Moving Somewhere")
#         move_arm(pub_command, loop_rate, Q[sr][sc][1], 4.0, 4.0)
#         move_arm(pub_command, loop_rate, Q[er][ec][1], 4.0, 4.0)
#         move_arm(pub_command, loop_rate, Q[er][ec][0], 4.0, 4.0)
#         rospy.loginfo("Dropping")
#         gripper(pub_command, loop_rate, suction_off)
#         move_arm(pub_command, loop_rate, Q[er][ec][1], 4.0, 4.0)
#     #print(Q[er][ec][1])
#     lab_fk(Q[er][ec][1][0],Q[er][ec][1][1],Q[er][ec][1][2],Q[er][ec][1][3],Q[er][ec][1][4],Q[er][ec][1][5])

#     return


def main():

    global home
    global Q
    global SPIN_RATE
    

    # Parser
    parser = argparse.ArgumentParser(description='Please specify if using simulator or real robot')
    parser.add_argument('--simulator', type=str, default='True')
    args = parser.parse_args()

    # Definition of our tower

    # 2D layers (top view)

    # Layer (Above blocks)
    # | Q[0][2][1] Q[1][2][1] Q[2][2][1] |   Above third block
    # | Q[0][1][1] Q[1][1][1] Q[2][1][1] |   Above point of second block
    # | Q[0][0][1] Q[1][0][1] Q[2][0][1] |   Above point of bottom block

    # Layer (Gripping blocks)
    # | Q[0][2][0] Q[1][2][0] Q[2][2][0] |   Contact point of third block
    # | Q[0][1][0] Q[1][1][0] Q[2][1][0] |   Contact point of second block
    # | Q[0][0][0] Q[1][0][0] Q[2][0][0] |   Contact point of bottom block

    # First index - From left to right position A, B, C
    # Second index - From "bottom" to "top" position 1, 2, 3
    # Third index - From gripper contact point to "in the air" point

    # How the arm will move (Suggestions)
    # 1. Go to the "above (start) block" position from its base position
    # 2. Drop to the "contact (start) block" position
    # 3. Rise back to the "above (start) block" position
    # 4. Move to the destination "above (end) block" position
    # 5. Drop to the corresponding "contact (end) block" position
    # 6. Rise back to the "above (end) block" position

    # Initialize rospack
    rospack = rospkg.RosPack()
    # Get path to yaml
    lab2_path = rospack.get_path('lab2pkg_py')
    yamlpath = os.path.join(lab2_path, 'scripts', 'lab2_data.yaml')

    with open(yamlpath, 'r') as f:
        try:
            # Load the data as a dict
            data = yaml.load(f)
            if args.simulator.lower() == 'true':
                Q = data['sim_pos']
            elif args.simulator.lower() == 'false':
                Q = data['real_pos']
            else:
                print("Invalid simulator argument, enter True or False")
                sys.exit()
            
        except:
            print("YAML not found")
            sys.exit()

    # Initialize ROS node
    rospy.init_node('lab2node')

    # Initialize publisher for ur3/command with buffer size of 10
    pub_command = rospy.Publisher('ur3/command', command, queue_size=10)

    # Initialize subscriber to ur3/position and callback fuction
    # each time data is published
    sub_position = rospy.Subscriber('ur3/position', position, position_callback)

    ############## Your Code Start Here ##############
    # TODO: define a ROS subscriber for ur3/gripper_input message and corresponding callback function


    sub_gripper_in = rospy.Subscriber('ur3/gripper_input', gripper_input, topic_callback)
    ic = ImageConverter(SPIN_RATE)


    ############### Your Code End Here ###############


    ############## Your Code Start Here ##############
    # TODO: modify the code below so that program can get user input

    input_done = 0
    loop_count = 1


    while(not input_done):
        input_string = raw_input("Ohh No! Your Sheep are in danger! Press 1 to save them 0 to Baaaaaah> ")
        print("You entered " + input_string + "\n") 

        if(int(input_string) == 1):
            input_done = 1
        elif (int(input_string) == 0):
            print("Baaaaaaaaaaah... ")
            sys.exit()
        else:
            print("Please just enter the 1 or 0 to Baaaaaaaah \n\n")

   

    ############### Your Code End Here ###############
    pub_command = rospy.Publisher('ur3/command', command, queue_size=10)
    vel = 6.0
    accel = 6.0
    # Check if ROS is ready for operation
    while(rospy.is_shutdown()):
        print("ROS is shutdown!")

    rospy.loginfo("Sending Goals ...")

    loop_rate = rospy.Rate(SPIN_RATE)

    global go_away
    global xw_yw_S
    global xw_yw_W
    global xw_yw_R

    # global variable1
    # global variable2
    #global sheep_dest
    global wolf_dest
    global pause
    sheep_dest = []
    for i in range(len(xw_yw_S)):
        sheep_dest.append([.2+i*.05, 0, .0318])
    wolf_dest = []
    for i in range(len(xw_yw_W)):
        sheep_dest.append([.2+i*.05, 0, .0318])
    ############## Your Code Start Here ##############
    # TODO: modify the code so that UR3 can move tower accordingly from user input
    move_arm(pub_command, loop_rate, go_away, vel, accel)
    time.sleep(1)
    g = 0
    j = 0
    danger = {}
    safe_used = []
    for i in xw_yw_G:
        safe_used.append(i)
    for sheep in xw_yw_S:
        vals = []
        for wolf in xw_yw_W:
            xsw = sheep[0] - wolf[0]
            ysw = sheep[1] - wolf[1]
            vals.append((xsw**2 + ysw**2)**.5)
        vals.sort()
        average = sum(vals) / len(vals)
        danger[tuple(sheep)] = average
    danger_sorted = []
    sorted_keys = sorted(danger, key=danger.get)
    for w in sorted_keys:
        danger_sorted.append(w)
    for sheep in danger_sorted:
        min_dist = 10000
        for safe in safe_used:
            xsw = sheep[0] - safe[0]
            ysw = sheep[1] - safe[1]
            curr_d = (xsw**2 + ysw**2)**.5
            if curr_d < min_dist :
                best = safe
                min_dist = curr_d
        blah = []
        for i in safe_used:
            if i[0] == best[0] and i[1] == best[1] and i[2] == best[2]:
                sheep_dest.append(0)
            else:
                blah.append(i)
        safe_used = blah
        move_block(pub_command, loop_rate, sheep, best, vel, accel)
        g+=1
    for wolf in xw_yw_W:
        move_block(pub_command, loop_rate, wolf, xw_yw_R[j], vel, accel)
        j+=1
    



    ############### Your Code End Here ###############


if __name__ == '__main__':

    try:
        main()
    # When Ctrl+C is executed, it catches the exception
    except rospy.ROSInterruptException:
        pass

