#!/usr/bin/env python

import rospy
import rospkg
import os
import sys
import yaml
import random
from gazebo_msgs.srv import SpawnModel
from gazebo_msgs.srv import DeleteModel
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Point
from geometry_msgs.msg import Quaternion
from std_srvs.srv import Empty


yamlpath = 'lab2_data.yaml'
#spawn_flag = 0

if __name__ == '__main__':

    # Initialize rospack
    rospack = rospkg.RosPack()
    # Get path to yaml
    lab2_path = rospack.get_path('lab2pkg_py')
    yamlpath = os.path.join(lab2_path, 'scripts', 'lab2_data.yaml')

    with open(yamlpath, 'r') as f:
        try:
            # Load the data as a dict
            data = yaml.load(f)
            # Load block position
            block_xy_pos = data['block_xy_pos']
            
        except:
            sys.exit()

    # Initialize ROS node
    rospy.init_node('ur3_gazebo_spawner', anonymous=True)
    # Initialize ROS pack
    rospack = rospkg.RosPack()
    # Get path to block
    ur_path = rospack.get_path('ur_description')
    block_path = os.path.join(ur_path, 'urdf', 'block.urdf')
    # wolves
    rl = 4 #forward and backward (x)
    cl = 9 #left and right (y)
    loc = []
    for r in range(rl):
        row =[]
        for c in range(cl):
            row.append((0.125+(0.04*r),0.05+(0.04*c)))
        loc.append(row)    
    # print(loc)

    srl = rl + 1 #forward and backward (x)
    scl = cl + 4 #left and right (y)
    sloc = []
    for r in range(srl):
        srow =[]
        for c in range(scl):
            srow.append((0.15+(0.036*r),-0.05+(0.04*c)))
        sloc.append(srow)    

    jr = 1
    jc = 4
    jail =[]
    for r in range(jr):
        jailrow =[]
        for c in range(jc):
            jailrow.append((0.0-(0.048*r),0.44+(0.04*c)))
        jail.append(jailrow)  

    #sheep

    sheep_p = os.path.join(ur_path, 'urdf', 'block_white.urdf')
    wolf_p = os.path.join(ur_path, 'urdf', 'block_black.urdf')
    wall_p = os.path.join(ur_path, 'urdf', 'block_green_flat.urdf')
    jail_p = os.path.join(ur_path, 'urdf', 'block_red_flat.urdf')
    wall_p_s2s = os.path.join(ur_path, 'urdf', 'walls2s.urdf')
    wall_p_f2b = os.path.join(ur_path, 'urdf', 'wallf2b.urdf')

    # Wait for service to start
    rospy.wait_for_service('gazebo/spawn_urdf_model')
    spawn = rospy.ServiceProxy('gazebo/spawn_urdf_model', SpawnModel)
    delete = rospy.ServiceProxy('gazebo/delete_model', DeleteModel)
    rospy.wait_for_service('/gazebo/reset_world')
    reset_world = rospy.ServiceProxy('/gazebo/resest_world', Empty)



    spawnordel = None
    while not spawnordel:
        spawnordel = raw_input("Enter Spawn(1) or Delete(2) or SpawnWalls(3) <Either 1 , 2, or 3>: ")
        spawnordel = int(spawnordel)
        if (spawnordel != 1) and (spawnordel != 2) and (spawnordel != 3):
            starting_location = None
            print("Wrong input \n\n")
    
    if spawnordel == 3:
        pose = Pose(Point(0, 0 ,0),Quaternion(0, 0, 0, 0))
        spawn('wall1', open(wall_p, 'r').read(), 'wall', pose, 'world')

    if spawnordel == 2:
        for r in range(rl):
            for c in range(cl):
                delete('block'+str(r)+str(c))
                delete('block'+str(r)+str(c))

    # will be randomized in future
    if spawnordel == 1:
        seed = raw_input("Enter Seed(Numbers): ") #Seed 11223344 is awesome, 6225
        seed= int(seed)
        random.seed(seed)
        sheep = raw_input("Enter # of Sheep To Spawn(Int): ")
        sheep= int(sheep)
        wolf = raw_input("Enter # of Wolves To Spawn(Int): ")
        wolf= int(wolf)
        
        # col 1
        sc = 1
        wc = 1
        safec = 1
        jailc =1
        while(sc <= sheep or wc <= wolf):
            for r in range(rl): #len(loc[0])
                for c in range(cl): #len(loc)
                    sf = random.getrandbits(5)
                    #print(sf)
                    if (sf) == 1:
                        if(sc <= sheep):
                            #print("Sheep Spawned")
                            pose = Pose(Point(loc[r][c][0],loc[r][c][1] ,0),Quaternion(0, 0, 0, 0))
                            spawn('block'+str(r)+str(c), open(sheep_p, 'r').read(), 'sheep', pose, 'world')
                            print("Sheep Spawned")
                            sc = sc + 1 
                            # else:
                            #     print("Sheep Failed")
                    if sf == 3:
                        if(wc <= wolf):
                            #print("Wolf Spawned")
                            pose = Pose(Point(loc[r][c][0],loc[r][c][1] ,0),Quaternion(0, 0, 0, 0))
                            spawn('block'+str(r)+str(c), open(wolf_p, 'r').read(), 'wolf', pose, 'world')
                            print("Wolf Spawned")
                            wc = wc + 1 
                            # else:
                            #     print("Wolf Failed")
                    
        while(safec <=sheep):
            for r in range(srl): #len(loc[0])
                    for c in range(scl): #len(loc)
                        sf = random.getrandbits(5)
                        if sf == 5:
                            if(safec <= sheep):
                                pose = Pose(Point(sloc[srl-1][c][0],sloc[srl-1][c][1] ,0),Quaternion(0, 0, 0, 0))
                                spawn('safe'+str(safec)+str("5"), open(wall_p, 'r').read(), 'safe', pose, 'world')
                                print("Safe Spawned5")
                                safec = safec+1
                        if sf == 7:
                            if(safec <= sheep):
                                pose = Pose(Point(sloc[r][scl-1][0],sloc[r][scl-1][1] ,0),Quaternion(0, 0, 0, 0))
                                spawn('safe'+str(safec)+str("7"), open(wall_p, 'r').read(), 'safe', pose, 'world')
                                print("Safe Spawned7")
                                safec = safec+1

                        if sf == 9:
                            if(safec <= sheep):
                                pose = Pose(Point(sloc[r][0][0],sloc[r][0][1] ,0),Quaternion(0, 0, 0, 0))
                                spawn('safe'+str(safec)+str("9"), open(wall_p, 'r').read(), 'safe', pose, 'world')
                                print("Safe Spawned9")
                                safec = safec+1
        
        # pose = Pose(Point(0.07, 0.45 ,0),Quaternion(0, 0, 0, 0))
        # spawn('wall1', open(wall_p_s2s, 'r').read(), 'wall', pose, 'world')
        
        # pose = Pose(Point(-0.15, 0.45 ,0),Quaternion(0, 0, 0, 0))
        # spawn('wall2', open(wall_p_s2s, 'r').read(), 'wall', pose, 'world')

        # pose = Pose(Point(-0.04, 0.35 ,0),Quaternion(0, 0, 0, 0))
        # spawn('wall3', open(wall_p_f2b, 'r').read(), 'wall', pose, 'world')
   
        # pose = Pose(Point(-0.04, 0.55 ,0),Quaternion(0, 0, 0, 0))
        # spawn('wall4', open(wall_p_f2b, 'r').read(), 'wall', pose, 'world')

        while(jailc <=wolf):
            for r in range(jr): #len(loc[0])
                    for c in range(jc): #len(loc)
                        #sf = random.getrandbits(6)
                        #if sf == 5:
                        if(jailc <= wolf):
                            pose = Pose(Point(jail[0][c][0],jail[0][c][1] ,0),Quaternion(0, 0, 0, 0))
                            spawn('jail'+str(jailc), open(jail_p, 'r').read(), 'jail', pose, 'world')
                            print("jailSpawned")
                            jailc = jailc+1

                


        #spawn_flag = 1